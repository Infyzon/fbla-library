import DS from 'ember-data';
import ENV from 'library/config/environment';

export default DS.Adapter.extend({
  _forward(name) {
    return function (...args) {
      return this._current[name](...args);
    }
  },

  init() {
    this._super(...arguments);
    this.set('adapters',
      {
        'sqlite': this.get('sqlite'),
        'server': this.get('server')
      }
    );

    if (window && window.process && window.process.type) {
      console.log("sqlite");
      this.set('_current', this.get('adapters').sqlite);
    } else {
      console.log("server");
      this.set('_current', this.get('adapters').server);
    }
  },
  findRecord: function (...args) {
    return this._current.findRecord(...args);
  },
  findAll: function (...args) {
    return this._current.findAll(...args);
  },
  query: function (...args) {
    return this._current.query(...args);
  },
  updateRecord: function (...args) {
    return this._current.updateRecord(...args);
  },
  deleteRecord: function (...args) {
    return this._current.deleteRecord(...args);
  }
});
