import DS from 'ember-data';
import RSVP from 'rsvp';
// eslint-disable-next-line no-undef
// const sqlite = requireNode('sqlite');
const Promise = RSVP.Promise;

const table_create = `
CREATE TABLE IF NOT EXISTS Author (
    id integer NOT NULL CONSTRAINT Author_pk PRIMARY KEY,
    name varchar(64) NOT NULL,
    description text NOT NULL
);
CREATE TABLE IF NOT EXISTS Book (
    id integer NOT NULL CONSTRAINT Book_pk PRIMARY KEY,
    name text NOT NULL,
    author_id integer NOT NULL,
    genre_id integer NOT NULL,
    CONSTRAINT BookInformation_Author FOREIGN KEY (author_id)
    REFERENCES Author (id),
    CONSTRAINT BookInformation_Genre FOREIGN KEY (genre_id)
    REFERENCES Genre (id)
);
CREATE TABLE IF NOT EXISTS Fine (
    id integer NOT NULL CONSTRAINT Fine_pk PRIMARY KEY,
    patron_id integer NOT NULL,
    amount integer NOT NULL,
    reason text NOT NULL,
    CONSTRAINT Fines_Patron FOREIGN KEY (patron_id)
    REFERENCES Patron (id)
);
CREATE TABLE IF NOT EXISTS Genre (
    id integer NOT NULL CONSTRAINT Genre_pk PRIMARY KEY,
    name varchar(32) NOT NULL
);
CREATE TABLE IF NOT EXISTS LibraryCopy (
    id integer NOT NULL CONSTRAINT LibraryCopy_pk PRIMARY KEY,
    book_id integer NOT NULL,
    due_date date,
    checked_out_on date,
    checked_out_by integer,
    CONSTRAINT Books_Patrons FOREIGN KEY (checked_out_by)
    REFERENCES Patron (id),
    CONSTRAINT LibraryBook_Book FOREIGN KEY (book_id)
    REFERENCES Book (id)
);
CREATE TABLE IF NOT EXISTS Patron (
    id integer NOT NULL CONSTRAINT Patron_pk PRIMARY KEY,
    name varchar(64) NOT NULL,
    patron_type_id integer NOT NULL,
    CONSTRAINT patrons_PatronType FOREIGN KEY (patron_type_id)
    REFERENCES PatronType (id)
);
CREATE TABLE IF NOT EXISTS PatronType (
    id integer NOT NULL CONSTRAINT PatronType_pk PRIMARY KEY,
    name varchar(32) NOT NULL,
    checkout_days integer NOT NULL,
    checkout_limit integer NOT NULL
);
`;

export default DS.Adapter.extend({
  init() {
    if (!(window && window.process && window.process.type)) {
      return;
    }

    this._super(...arguments);
    this.set('ready', false);

    const context = this;
    sqlite.open('../../db.sqlite', {Promise})
      .then(function (db) {
        context.set('db', db);
        console.log(context.get('db'));
        return db.exec(table_create, {Promise});
      }).then(function () {
      context.set('ready', true);
    });
  },
  _getAllRowsWithID(db, tableName) {
    return db.prepare(`SELECT * FROM ${tableName.classify()} WHERE id = (?)`, {Promise});
  },
  _getAllRows(db, tableName) {
    return db.prepare(`SELECT * FROM ${tableName.classify()}`, {Promise});
  },
  findRecord(store, type, id) {
    const db = this.get('db');
    return this._getAllRowsWithID(db, type.modelName).then(function (result) {
      return result.get([id], {Promise});
    });
  },
  findAll(store, type) {
    return this._getAllRows(this.get('db'), type.modelName).then(function (result) {
      return result.all({Promise});
    });
  },
  query(store, type) {
    return this.findAll(store, type);
  },
  updateRecord(store, type, snapshot) {

  }
});
