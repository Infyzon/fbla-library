import DS from 'ember-data';
import Ember from 'ember';

export default DS.RESTAdapter.extend({
  host: 'http://sharath.pro:10095',
  pathForType(type) {
    const camelized = Ember.String.camelize(type);
    return Ember.String.singularize(camelized);
  }
});
