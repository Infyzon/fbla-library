import {get} from '@ember/object';
import DS from 'ember-data';

export default DS.Serializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    // console.log(get(primaryModelClass, 'relationshipsByName'));
    // console.log(get(primaryModelClass, 'relationships'));
    let dict = this.get('dictionary');
    if (!dict) {
      this.set('dictionary', {
        "findRecord": this._normalizeFindRecord,
        "findAll": this._normalizeFindAll,
        "query": this._normalizeFindAll
      });
      dict = this.get('dictionary');
    }

    return {
      data: dict[requestType](this, store, primaryModelClass, payload, id)
    };
  },
  _normalizeFindAll(context, store, primaryModelClass, payload) {
    return payload.map(function (row) {
      return context['_normalizeFindRecord'](context, store, primaryModelClass, row);
    });
  },
  _normalizeFindRecord(context, store, primaryModelClass, payload) {
    let obj = {
      id: payload['id'],
      type: get(primaryModelClass, 'modelName'),
      attributes: {},
      relationships: {
        patronType: {
          data: {
            id: payload['patron_type_id'],
            type: 'patronType'
          }
        }
      }
    };
    let fields = get(primaryModelClass, 'fields');
    fields.forEach(function (kind, field) {
      if (payload.hasOwnProperty(field)) {
        obj['attributes'][field] = payload[field];
      }
    });
    return obj;
  }
});
