import RSVP from 'rsvp';

export function initialize(appInstance) {
  if (!(window && window.process && window.process.type)) {
    return;
  }
  let adapter = appInstance.lookup('adapter:sqlite');
  return new RSVP.Promise(function (resolve) {
    while (!adapter.get('ready')) {
      console.log(adapter);
    }
    resolve();
  });
}

export default {
  initialize
};
