export function initialize(appInstance) {
  if (!(window && window.process && window.process.type)) {
    console.log("anakwenakwenkwae");
    appInstance.inject('adapter:application', 'sqlite', 'adapter:sqlite');
  }
  appInstance.inject('adapter:application', 'server', 'adapter:server');
}

export default {
  after: 'sqlite-adapter',
  initialize
};
