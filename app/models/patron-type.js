import DS from 'ember-data';
import {computed} from '@ember/object';

export default DS.Model.extend({
  name: DS.attr('string'),
  friendlyName: computed('name', function () {
    return this.get('name').capitalize();
  }),
  checkout_days: DS.attr('number'),
  checkout_limit: DS.attr('number')
});
