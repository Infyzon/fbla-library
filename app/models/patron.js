import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  patronType: DS.belongsTo('patron-type')
});
