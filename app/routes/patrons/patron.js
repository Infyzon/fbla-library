import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model(params) {
    console.log(this.modelFor('patrons'));
    const context = this;
    let promises = {
      patron: this.modelFor('patrons')[0],
      patronTypes: context.get('store').findAll('patron-type')
    };
    return RSVP.hash(promises);
  }
});
