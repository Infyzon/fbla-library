import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function () {
  this.route('patrons', function () {
    this.route('patron', {path: 'patron/:patron_id'});
    this.route('types', {path: 'types/:patron_type_id'});
  });
});

export default Router;
