import Controller from '@ember/controller';
import {inject as service} from '@ember/service';

export default Controller.extend({
  paperToaster: service(),
  showDeleteDialog: true,
  actions: {
    deletePatron(patron) {
      const paperToaster = this.get('paperToaster');
      const context = this;
      return patron.destroyRecord().then(() => {
        paperToaster.show("Patron deleted", {
          duration: 4000
        });
        context.transitionToRoute("patrons");
      });
    },
    savePatron(patron) {
      const paperToaster = this.get('paperToaster');
      patron.send('becomeDirty');
      return patron.save().then(function () {
        paperToaster.show("Patron updated", {
          duration: 40000
        });
      });
    }
  }
});
